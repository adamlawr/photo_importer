#!/usr/bin/env ruby
require 'fileutils'
require 'pry'
require 'optparse'
require 'mini_exiftool'

module Raceweb
  module PhotoImporter
    class Renamer
      BATCH_SIZE = 100
      EXIF_DATE_FIELDS = %w[datetime datetimeoriginal creationdate filemodifydate mediacreatedate].freeze
      FILE_NAME_DATE_FORMAT = '%Y%m%d_%H%M'.freeze
      KNOWN_ACTIONS = %w[:import :report :put_exif].freeze
      KNOWN_REPORTS = %i[wrong_name has_exif exif].freeze
      INDEX_SEPERATOR = '_'.freeze

      # if we're getting the exif time from the filename, this is how we find it and turn it into a time
      # 2018-01-01 12;30.jpg
      # FILENAME_TIME_REGEXP = '(\d{4}-\d{2}-\d{2} \d{2};\d{2})'.freeze
      # FILENAME_TIME_FORMAT = '%Y-%m-%d %H;%M'.freeze
      # 20180101_1234_001.jpg
      FILENAME_TIME_REGEXP = '(\d{4}\d{2}\d{2}_\d{2}\d{2})'.freeze
      FILENAME_TIME_FORMAT = '%Y%m%d_%k%M'.freeze
      # 20180101_001.jpg
      # FILENAME_TIME_REGEXP = '(\d{4}\d{2}\d{2})'.freeze
      # FILENAME_TIME_FORMAT = '%Y%m%d'.freeze

      def initialize(options)
        @action = options[:action]
        @source = options[:source]
        @target = options[:target]
        @mask = options[:mask]
        @date = options[:date]
        @ext = options[:ext]
        @pretending = options[:pretend]
        @report = options[:report]
        @year_sorting = options[:year].to_i
        @fntr = options[:filename_time_regexp]
        @fntf = options[:filename_time_format]
      end

      ###########################################
      # actions
      ###########################################
      def write_exif
        count = 0
        start = Time.now
        files = Dir["#{@source}/*#{@mask}*.#{@ext}"].sort
        puts "writing exif time for #{files.count} files with #{@date ? @date : 'filenames'}"
        files.each_slice(BATCH_SIZE) do |slice|
          slice.each do |name|
            count += 1
            photo = MiniExiftool.new(name)
            photo.datetime = if @date
                               # get date from parameter
                               Time.parse(@date)
                             else
                               # get date from file name
                               r = Regexp.new(@fntr)
                               filename_time = r.match(File.basename(name, '.*'))
                               Time.strptime(filename_time[1], @fntf)
                             end
            puts "#{count} saving #{name} with #{photo.datetime} #{@pretending ? '(just practicing)' : ''}"
            photo.save unless @pretending
          end
          puts "wrote exif date for #{count} files in #{seconds_to_time(Time.now - start)}"
        end
      end

      def report
        raise 'unknown report' unless known_report?(@report)
        count = 0
        start = Time.now
        files = Dir["#{@source}/*#{@mask}*.#{@ext}"].sort
        puts "reporting on #{@report} for #{files.count} files"
        files.each_slice(BATCH_SIZE) do |slice|
          slice.each do |full_file_name|
            puts full_file_name
            count += 1
            case @report
            when :wrong_name
              report_wrong_name(count, full_file_name)
            when :has_exif
              report_has_exif(count, full_file_name)
            when :exif
              report_exif(count, full_file_name)
            end
          end
          puts "reported on #{count} files in #{seconds_to_time(Time.now - start)}"
        end
      end

      def report_wrong_name(count, full_file_name)
        date_from_exif = formatted_date(full_file_name)
        date_from_filename = File.basename(full_file_name, '.*').match(/(.*)_/)[1]
        puts "#{count} wrong: #{full_file_name} date: #{date_from_exif}" if date_from_filename != date_from_exif
      end

      def report_has_exif(count, full_file_name)
        date_from_exif = formatted_date(full_file_name)
        puts "#{count} no exif date: #{full_file_name}" if date_from_exif == 'unknown'
      end

      def report_exif(count, full_file_name)
        exif_data = MiniExiftool.new(full_file_name)
        puts "######################################################################"
        puts "#{count}. exif data for: #{full_file_name}"
        puts "   found #{exif_data.tags.compact.count} tags"
        puts "######################################################################"
        exif_data.tags.compact.sort.each do |tag|
          puts tag.ljust(28) + exif_data[tag].to_s
        end
      end

      def import
        raise 'folder not found' unless Dir.exist?(@source) && Dir.exist?(@target)
        count = 0
        start = Time.now
        files = Dir[File.expand_path("#{@source}/*#{@mask}*.#{@ext}")]
        puts "importing #{files.count} files..."

        sorted = []
        files.each do |file|
          sorted << [file, formatted_date(file)]
        end
        sorted.sort! { |x, y| x[1] <=> y[1] } # sort by the formatted exif date

        sorted.each_slice(BATCH_SIZE) do |slice|
          slice.each do |sorted_file|
            count += 1
            new_name = new_name(sorted_file[0])
            if @source == @target
              puts "#{count} renaming #{sorted_file[0]} to #{new_name} #{@pretending ? '(just practicing)' : ''}"
              FileUtils.move(sorted_file[0], new_name) unless @pretending
            else
              puts "#{count} copying #{sorted_file[0]} to #{new_name} #{@pretending ? '(just practicing)' : ''}"
              FileUtils.copy(sorted_file[0], new_name) unless @pretending
            end
          end
          puts "imported #{count} files in #{seconds_to_time(Time.now - start)}"
        end
      end

      def custom
        count = 0
        start = Time.now
        files = Dir["#{@source}/*#{@mask}*.#{@ext}"].sort
        puts "custom action for #{files.count} files"
        files.each_slice(BATCH_SIZE) do |slice|
          slice.each do |full_file_name|
            count += 1
            photo = MiniExiftool.new(full_file_name)
            d = photo.send('datetime')
            d = Date.strptime(d, '%Y:%m:%d') if photo.instance_of? String
            t = photo.send('datetimeoriginal')
            f = DateTime.new(d.year, d.month, d.day, t.hour, t.min, t.sec, t.zone)
            puts "#{count} custom #{full_file_name} #{f} #{@pretending ? '(just practicing)' : ''}"
            photo.datetime = f
            photo.save unless @pretending
          end
          puts "custom action run on #{count} files in #{seconds_to_time(Time.now - start)}"
        end
      end
      ###########################################
      # end actions
      ###########################################

      def known_report?(report)
        KNOWN_REPORTS.include?(report)
      end

      def get_source
        puts 'where are the photos?'
        @source = gets.chomp
        raise 'no source given' if @source.empty?
      end

      def get_target
        puts 'where are the photos going?'
        @target = gets.chomp
        raise 'no target given' if @target.empty?
      end

      private

      def folder(formatted_date)
        return @target if @year_sorting == 0
        year = formatted_date[0, 4].to_i
        "#{@target}/#{(year - year % @year_sorting)}"
      end

      def new_name(full_file_name)
        formatted_date = formatted_date(full_file_name)
        folder = folder(formatted_date)
        sequence = next_sequence_for(folder, formatted_date)
        "#{folder}/#{formatted_date}#{INDEX_SEPERATOR}#{sequence.to_s.rjust(3, '0')}#{File.extname(full_file_name).downcase}"
      end

      def formatted_date(full_file_name)
        begin
          exif_date = date_from_file(full_file_name)
          exif_date = Date.strptime(exif_date, '%Y:%m:%d') if exif_date.instance_of? String
        rescue StandardError => e
          puts "can't get exif date for file #{full_file_name} because #{e}"
          exif_date = nil
        end
        exif_date ? exif_date.strftime(FILE_NAME_DATE_FORMAT) : 'unknown'
      end

      def date_from_file(full_file_name)
        exif_data = MiniExiftool.new(full_file_name)
        exif_date = nil
        EXIF_DATE_FIELDS.each do |field|
          break if exif_date
          value = exif_data.send(field)
          next unless value # && value.class == Time
          exif_date = value
        end
        exif_date
      end

      def next_sequence_for(folder, file_name)
        sequence = 1
        Dir[File.expand_path("#{folder}/#{file_name}#{INDEX_SEPERATOR}*.#{@ext.downcase}")].each do |full_file_name|
          basename = File.basename(full_file_name, '.*') # without extension
          current_seq = basename.match(/_(\d*)$/)[1].to_i
          sequence = [sequence, current_seq].max + 1
        end
        sequence
      end

      # just for reporting
      def seconds_to_time(seconds)
        seconds = seconds.to_i
        [seconds / 3600, seconds / 60 % 60, seconds % 60].map { |t| t.to_s.rjust(2, '0') }.join(':')
      end
    end
  end
end
