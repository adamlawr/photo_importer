lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "raceweb/photo_importer/version"

Gem::Specification.new do |spec|
  spec.name          = "photo_importer"
  spec.version       = Raceweb::PhotoImporter::VERSION
  spec.authors       = ["Adam Lawrence"]
  spec.email         = ["adam@raceweb.ca"]

  spec.summary       = %q{import and rename photos and videos}
  spec.description   = %q{import and rename photos and videos from exif data}
  spec.homepage      = "https://gitlab.com/adamlawr/photo_importer"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

    spec.metadata["homepage_uri"] = spec.homepage
    spec.metadata["source_code_uri"] = spec.homepage
    spec.metadata["changelog_uri"] = "https://gitlab.com/adamlawr/photo_importer"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'bin'
  spec.executables   = ['photo-importer']
  spec.require_paths = ['lib']

  spec.add_development_dependency "bundler", "~> 2.2.17"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
  spec.add_development_dependency 'pry', '~> 0.14.2'
  spec.add_development_dependency 'pry-byebug', '~> 3.6', '>= 3.6.0'
  spec.add_dependency 'mini_exiftool'
end
