# PhotoImporter

import and organize photos based on exif data

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/photo_importer`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'photo_importer'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install photo_importer

### dependencies

exiftool
- download from [here](http://www.sno.phy.queensu.ca/~phil/exiftool/install.html)
- extract the executable to a directory in the system path
- test with `exiftool -ver`

## Usage

run with
```
$ bundle exec photo-importer -h
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome at [gitlab](https://gitlab.com/adamlawr/photo_importer).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
