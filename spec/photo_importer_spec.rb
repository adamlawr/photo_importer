require 'spec_helper'

class PhotoImporterSpec < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil Raceweb::PhotoImporter::VERSION
    assert_equal '0.1.0', Raceweb::PhotoImporter::VERSION
  end

  def test_it_does_something_useful
    assert true
  end

  describe Raceweb::PhotoImporter::Renamer do
    let(:default_options) {
      { action: :import, report: :wrong_name, source: nil, target: nil,
      mask: nil, ext: '*', date: nil, full: nil, pretend: false, year: 0,
      filename_time_regexp: Raceweb::PhotoImporter::Renamer::FILENAME_TIME_REGEXP, filename_time_format: Raceweb::PhotoImporter::Renamer::FILENAME_TIME_FORMAT }
    }

    # describe '#get_source' do
    #   it 'requires source images' do
    #     assert_raises('no source given') {
    #       options = default_options.merge({ source: 'c:\app\pesonal\photo_importer\spec\img' })
    #       renamer = Raceweb::PhotoImporter::Renamer.new(options)
    #       rename.get_source
    #     }
    #   end
    # end

    describe '#report' do
      it 'complains about an unkown report' do
        options = default_options.merge({ report: 'foobar' })
        renamer = Raceweb::PhotoImporter::Renamer.new(options)
        err = assert_raises(RuntimeError) { renamer.report }
        assert_match /unknown report/, err.message
      end

      it 'runs a report' do
        options = default_options.merge({ report: :wrong_name })
        renamer = Raceweb::PhotoImporter::Renamer.new(options)
        renamer.report
      end

      it 'return a report' do
        renamer = Raceweb::PhotoImporter::Renamer.new(default_options)

        assert_equal 2, 2
      end
    end
  end
end
