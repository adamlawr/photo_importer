require 'optparse'
require 'raceweb/photo_importer/renamer'
require 'pry'

module Raceweb
  module PhotoImporter
    class Cli
      def initialize
        default_options = {
          action: :import, report: :wrong_name, source: nil, target: nil,
          mask: nil, ext: '*', date: nil, full: nil, pretend: false, year: 0,
          filename_time_regexp: Renamer::FILENAME_TIME_REGEXP, filename_time_format: Renamer::FILENAME_TIME_FORMAT
        }
        @options = default_options.merge!(cli_options)
        # symbolize the keys in @options
        # @options = Raceweb::Utils.symbolize(@options).freeze
      end

      def start
        puts 'starting with options:'
        pp @options
        puts "action params: #{ARGV}"
        # if @options.key? :pretend
        #   puts 'just pretending. see ya.'
        #   return
        # end

        begin
          renamer = Raceweb::PhotoImporter::Renamer.new(@options)
          return unless renamer
          case @options[:action]
          when :import
            renamer.get_source unless @options[:source]
            renamer.get_target unless @options[:target]
            renamer.import
          when :report
            renamer.get_source unless @options[:source]
            renamer.report
          when :put_exif
            renamer.get_source unless @options[:source]
            renamer.write_exif
          when :custom
            renamer.get_source unless @options[:source]
            renamer.custom
          else
            raise 'unknown action'
          end
        rescue StandardError => e
          puts "error: #{e}"
          pp e.backtrace
          puts "options used: #{@options}"
        end
      end

      def help_banner
        <<~HEREDOC
        usage: photo-renamer.rb [options]
        actions:
          import (default): requires -s,-t
          report: requires -s and -r  wrong_name (default), has_exif, exif
          put_exif: requires -s. uses -d or the filename as the new exif time
          custom: requires -s. calls Renamer::custom
        HEREDOC
      end

      def cli_options
        cli_options = {}
        OptionParser.new do |opts|
          opts.banner = help_banner

          opts.on('-a ACTION', '--action ACTION', 'what are we doing here?') do |a|
            cli_options[:action] = a.to_sym
          end
          opts.on('-d', '--date=DATE', 'what\'s the new date?') do |d|
            cli_options[:date] = d
          end
          opts.on('-e', '--ext=EXTENSION', 'what kind of files?') do |e|
            cli_options[:ext] = e
          end
          opts.on('-f', '--full=FULL', 'are we guessing all defaults from one string?') do |f|
            cli_options[:full] = f
          end
          opts.on('-h', '--help', 'need some help?') do
            puts opts
            exit
          end
          opts.on('-m', '--mask=MASK', 'what files are we going to update?') do |m|
            cli_options[:mask] = m
          end
          opts.on('-p', '--pretend', 'are we just fooling around?') do |p|
            cli_options[:pretend] = p
          end
          opts.on('-r REPORT', '--report=REPORT', 'what report to run?') do |r|
            cli_options[:report] = r.to_sym
          end
          opts.on('-s', '--source=SOURCE', 'where are the photos now?') do |s|
            cli_options[:source] = s
          end
          opts.on('-t', '--target=TARGET', 'where are the photos going?') do |t|
            cli_options[:target] = t
          end
          opts.on('-y', '--year=YEAR', 'put into x year sub folders') do |y|
            cli_options[:year] = y
          end
        end.parse!
        cli_options
      end
    end
  end
end
